# @4geit/ngx-page-not-found-component [![npm version](//badge.fury.io/js/@4geit%2Fngx-page-not-found-component.svg)](//badge.fury.io/js/@4geit%2Fngx-page-not-found-component)

---

Dummy page not found component.

## Installation

1. A recommended way to install ***@4geit/ngx-page-not-found-component*** is through [npm](//www.npmjs.com/search?q=@4geit/ngx-page-not-found-component) package manager using the following command:

```bash
npm i @4geit/ngx-page-not-found-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-page-not-found-component
```

2. You need to import the `NgxPageNotFoundComponent` component within the module you want it to be. For instance `app.module.ts` as follows:

```js
import { NgxPageNotFoundComponent } from '@4geit/ngx-page-not-found-component';
```

And you also need to add the `NgxPageNotFoundComponent` component with the `@NgModule` decorator as part of the `declarations` list.

```js
@NgModule({
  // ...
  declarations: [
    // ...
    NgxPageNotFoundComponent,
    // ...
  ],
  // ...
})
export class AppModule { }
```

3. You need to import the `NgxPageNotFoundComponent` component in the routing file you want to use it. For instance `app-routing.module.ts` as follows:

```js
import { NgxPageNotFoundComponent } from '@4geit/ngx-page-not-found-component';
```

And you also need to add the `NgxPageNotFoundComponent` component within the list of `routes` as follows:

```js
const routes: Routes = [
  // ...
  { path: '**', component: NgxPageNotFoundComponent }
  // ...
];
```

Therefore if an non-existent route is requested the app will display the `NgxPageNotFoundComponent` template.
